#include <stdio.h>
// More information about stdio, execute: man stdio or info stdio.
// Read more about other '%' format characters that you can use, execute: man 3 printf.
int main() {
    int age=10;
    int height=72;

    // Using %s (String), %d (Integer).
    printf("I am %d years old.\n", age);
    printf("I amd %d inches tall.\n", height);

    return 0;
}
