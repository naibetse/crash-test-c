### 
# USAGE EX:
# make || make all (It's the same thing)
# make clean (rm -f)
# make clean all (rm -f && make)
###

# Automatize the command: CFLAGS="-Wall" make
# Wall displays the process compile
CFLAGS = -Wall -g

all: 
	make ex1 
	make ex3
clean:
	rm -f ex1
	rm -f ex3

# You can create targets like this:
# Execute: make test
test:
	echo "HELLO COMMAND!"
