#include <stdio.h>

/* This is a multiline comment 
 * as you can see.
 */
int main(int argc, char *argv[]) {
    int distance = 100;

    // This also a comment, for one line.
    printf("You are %d miles away.\n", distance);

    return 0;
}
